﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ITMore.Store.Startup))]
namespace ITMore.Store
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
